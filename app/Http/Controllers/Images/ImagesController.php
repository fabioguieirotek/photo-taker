<?php


namespace App\Http\Controllers\Images;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ImagesController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function saveImage(Request $request){
        $status = True;
        $message = "Success";
        try{
            $imageBase64 = $request->get('image');
            if($imageBase64==NULL) throw new Exception('Imagem não enviada. Parametro: image');
            $now = new Carbon();
            $bin = base64_decode($imageBase64);        
            $size = getImageSizeFromString($bin);
            if (empty($size['mime']) || strpos($size['mime'], 'image/') !== 0) throw new Exception('Base64 value is not a valid image');

            $img_file = "..\\storage\\images\\".$now->format('Y-m-d-H-i-s-u').".jpg";
            file_put_contents($img_file, $bin);

            return ["status"=>$status,
                    "message"=>$message];
            
        }
        catch(Exception $e){
            return ["status"=>FALSE,
                    "message"=>$e];
        }

        return $message;
    }
}
