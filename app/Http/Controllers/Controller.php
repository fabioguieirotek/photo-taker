<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Carbon\Carbon;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function index()
    {
        return "Home page";
    }

    public function createAnnotation(Request $request) {
        try {
            $imageData = $request->get('imageData');            
            /*
                @param ImageData (Object) É a variavel que contém as informações relativas à imagem que será anotada. Deverá conter as propriedades:
                folder: pasta onde a imagem está salva
                fileName: o nome do arquivo da imagem
                path: o diretório completo onde a imagem está salva
                widht: largura em px da imagem
                height: altura em px da imagem
                label: a label do objeto que será anotado
                xmin: o menor valor do eixo X da bounding box
                ymin: o menor valor do eixo Y da bounding box
                xmax: o maior valor do eixo X da bounding box
                ymax: o maior valor do eixo Y da bounding box
            */
            $annotationTemplate = 
            "<annotation>" 
                ."\n  <folder>FOLDER__NAME</folder>"
                ."\n  <filename>FILE__NAME</filename>"
                ."\n  <path>FULL__PATH</path>"
                ."\n  <source>"
                ."\n      <database>Unknown</database>"
                ."\n  </source>"
                ."\n  <size>"
                ."\n      <width>PICUTRE__WIDTH</width>"
                ."\n      <height>PICTURE__HEIGHT</height>"
                ."\n      <depth>3</depth>"
                ."\n  </size>"
                ."\n  <segmented>0</segmented>"
                ."\n  <object>"
                ."\n      <name>OBJECT__LABEL</name>"
                ."\n      <pose>Unspecified</pose>"
                ."\n      <truncated>0</truncated>"
                ."\n      <difficult>0</difficult>"
                ."\n      <bndbox>"
                ."\n          <xmin>X__MIN</xmin>"
                ."\n          <ymin>Y__MIN</ymin>"
                ."\n          <xmax>X__MAX</xmax>"
                ."\n          <ymax>Y__MAX</ymax>"
                ."\n      </bndbox>"
                ."\n    </object>"
            ."\n</annotation>";            
            $annotationTemplate = str_replace("FOLDER__NAME", $imageData['folder'], $annotationTemplate);
            $annotationTemplate = str_replace("FILE__NAME", $imageData['fileName'], $annotationTemplate);
            $annotationTemplate = str_replace("FULL__PATH", $imageData['path'], $annotationTemplate);
            $annotationTemplate = str_replace("PICUTRE__WIDTH", $imageData['width'], $annotationTemplate);
            $annotationTemplate = str_replace("PICTURE__HEIGHT", $imageData['height'], $annotationTemplate);
            $annotationTemplate = str_replace("OBJECT__LABEL", $imageData['label'], $annotationTemplate);
            $annotationTemplate = str_replace("X__MIN", $imageData['xmin'], $annotationTemplate);
            $annotationTemplate = str_replace("Y__MIN", $imageData['ymin'], $annotationTemplate);
            $annotationTemplate = str_replace("X__MAX", $imageData['xmax'], $annotationTemplate);
            $annotationTemplate = str_replace("Y__MAX", $imageData['ymax'], $annotationTemplate);

            $annotationPath = "..\\storage\\annotations\\";
            $annotationFile = $annotationPath . $imageData['fileName'] . ".xml";
            if(!is_dir($annotationPath)) mkdir($annotationPath);
            file_put_contents($annotationFile, $annotationTemplate);
            
            return array(
                "error" => False,
                "message" => "Annotation created with success"
            );
        } catch (Exception $e) {
            return array(
                "error" => true,
                "message" => $e
            );
        }
    }

}
