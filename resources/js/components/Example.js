import React, { useRef } from "react";
import Webcam from "react-webcam"
import ReactDOM from 'react-dom';
import './Example.css'

function Example() {
  const webcamRef = React.useRef(null);
  const [imgSrc, setImgSrc] = React.useState(null);

  const capture = React.useCallback(() => {
    const imageSrc = webcamRef.current.getScreenshot();
    setImgSrc(imageSrc);
  }, [webcamRef, setImgSrc]);

  return (
    <div className="content">

      <div className="darkbg" >
        <Webcam
          width={650}
          heigh={450}
          audio={false}
          ref={webcamRef}
          screenshotFormat="image/jpeg"
        />
        {imgSrc && (
          <img
            src={imgSrc}
          />
        )}
      </div>
      <div className="button-container">
        <button onClick={capture} className="button">CAPTURAR</button>
        <button className="button">MARCAR E ENVIAR</button>
      </div>
    </div>
  );
}

export default Example;

if (document.getElementById('example')) {
  ReactDOM.render(<Example />, document.getElementById('example'));
}
