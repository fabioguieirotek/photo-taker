
import React, { useRef } from "react";
import Webcam from "react-webcam";

const WebcamComponent = () => {
        const webcamRef = React.useRef(null);
        const [imgSrc, setImgSrc] = React.useState(null);
      
        const capture = React.useCallback(() => {
          const imageSrc = webcamRef.current.getScreenshot();
          setImgSrc(imageSrc);
        }, [webcamRef, setImgSrc]);
      
        return (
          <div>
            <Webcam
              audio={false}
              ref={webcamRef}
              screenshotFormat="image/jpeg"
            />
            <button onClick={capture}>Capture photo</button>
            {imgSrc && (
              <img
                src={imgSrc}
              />
            )}
          </div>
        );
      };
    
export default WebcamComponent











